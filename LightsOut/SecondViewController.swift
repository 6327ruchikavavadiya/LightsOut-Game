//
//  SecondViewController.swift
//  LightsOut
//
//  Created by ruchika vavadiya on 16/09/22.
//

import UIKit

class SecondViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var myCollectionView: UICollectionView!
    var arr = Array(repeating: Array(repeating: 0, count: 5), count: 5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        
        
        let range = 1...10
        for _ in 0..<range.randomElement()! {
            random()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = myCollectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! myCell
        if arr[indexPath.section][indexPath.row] == 1 {
            cell.layer.backgroundColor = #colorLiteral(red: 0.6056759357, green: 0.8216920495, blue: 0.5728448629, alpha: 1)
        } else {
            cell.layer.backgroundColor = #colorLiteral(red: 0.01575877704, green: 0.2711295187, blue: 0.1311040521, alpha: 1)
        }
        cell.layer.cornerRadius = 10
        cell.mylbl.text = arr[indexPath.section][indexPath.row].description
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        check(indexPath)
        myCollectionView.reloadData()
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var sizeofcell = 0
        sizeofcell = (Int(myCollectionView.frame.width)-(5+1)*15)/5
        return .init(width: sizeofcell, height: sizeofcell)
    }
    
    func random(){
        let array = (0...5-1)
        let i = array.randomElement()!
        let j = array.randomElement()!
        
        check(IndexPath(row: j, section: i))
    }
    
    func check(_ i : IndexPath) {
        let indexpaths = i
        let x = indexpaths.section
        let y = indexpaths.row
        
        let index = [[x,y],[x,y+1],[x,y-1],[x+1,y],[x-1,y]]
        
        for i in index {
            if i[0] > 4 || i[0] < 0 || i[1] > 4 || i[1] < 0 {
                continue
            } else {
                if arr[i[0]][i[1]] == 0 {
                    arr[i[0]][i[1]] = 1
                } else {
                    arr[i[0]][i[1]] = 0
                }
            }
        }
    }
}
