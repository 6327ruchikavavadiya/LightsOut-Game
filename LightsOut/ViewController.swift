//
//  ViewController.swift
//  LightsOut
//
//  Created by ruchika vavadiya on 15/09/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var mySlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mySlider.isHidden = true
        playBtn.layer.cornerRadius = 12
    }
    
    @IBAction func myPlayBtn(_ sender: UIButton) {
        mySlider.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
            self.mySlider.value = 1
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                self.mySlider.value = 2
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                    self.mySlider.value = 3
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2){
                        self.mySlider.value = 4
                        self.navigationController?.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: "SecondViewController"), animated: true)
                    }
                }
            }
        }
    }
}

